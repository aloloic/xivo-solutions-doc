.. _upgrade:

*********
Upgrading
*********

Upgrading a *XiVO PBX* is done by executing commands through a terminal on the
server. You can connect to the server either through SSH or with a physical
console.


Overview
========

To upgrade your *XiVO PBX*, you have to use both ``xivo-dist`` and :ref:`xivo-upgrade <xivo-upgrade_script>` tools.
These tools handle the process of upgrading:

* the system (Debian packages)
* and the *XiVO PBX* packages

.. warning:: The following applies to *XiVO PBX* **>= 2016.03**. For older version, see :ref:`version_specific_upgrade` section.

There are three cases:

#. :ref:`Upgrade to the latest XiVO PBX version <upgrade_latest_version_xpbx>`,
#. :ref:`Upgrade to a specific XiVO PBX version <upgrade_specific_version_xpbx>` (or archive version),
#. :ref:`Upgrade to the latest subversion <upgrade_latest_subversion_xpbx>` of your current installed version.


.. _upgrade_latest_version_xpbx:

Upgrade to **latest** version
-----------------------------

To upgrade to the latest version of 2017.LTS1, you have to update the sources list to point to *xivo-five*. You have to do it with the command ``xivo-dist`` supplied in *XiVO PBX*::

	xivo-dist xivo-five

or::

    xivo-dist xivo-2017.03-latest


.. _upgrade_specific_version_xpbx:

Upgrade to **specific** version
-------------------------------

To upgrade to a **specific** version, you have to update the sources to point to this *specific* version. You have to do it with the ``xivo-dist`` command. For example, if you want to upgrade to **2016.04**::

	xivo-dist xivo-2016.04-latest


.. _upgrade_latest_subversion_xpbx:

Upgrade to latest **subversion**
--------------------------------

When you are already in a *specific* version (e.g. 2016.04), you may want to upgrade to the **latest subversion** of this release (e.g. 2016.04.03) to benefit of the latest *fixes* shipped in this version. You have to do it with the ``xivo-dist`` command. For example, if you want to upgrade to the latest subversion of 2016.04 version::

	xivo-dist xivo-2016.04-latest


Preparing the upgrade
=====================

To prepare the upgrade you should:

#. Read :ref:`xivosolutions_release` starting from your version to the version you target.
#. Check if you are in a specific setup that requires a :ref:`specific procedure <upgrade_specific_proc>` 
   to be followed (e.g. :ref:`upgrading-a-cluster`).
#. Finally, you can download the packages beforehand by running ``xivo-upgrade -d``. This is not mandatory, 
   but it does not require stopping any service, so it may be useful to reduce the downtime of the server 
   while upgrading.


Upgrade
=======

#. For custom setups, follow the required procedures described below (e.g. :ref:`upgrading-a-cluster`).
#. When ready, launch the upgrade process. **All XiVO PBX services will be stopped during the process**::

	xivo-upgrade

#. When finished, check that all services are running (the list is displayed at the end of the upgrade).
#. Check that services are correctly working like SIP registration, ISDN link status,
   internal/incoming/outgoing calls, XiVO Client connections etc.


.. _upgrade_specific_proc:

Specific procedures
===================

.. toctree::
   :maxdepth: 1

   cluster
   migrate_i386_to_amd64
   asterisk_latest
   xivocc_recording


.. _version_specific_upgrade:

Version-specific upgrade procedures
===================================

.. note:: If your *XiVO PBX* is **below 2016.03** you have first to :ref:`switch-to-xivo.solutions` mirrors.

.. toctree::
   :maxdepth: 2

   switch_xivosolutions
   other_version_specific


Upgrading to/from an archive version
------------------------------------

.. toctree::
   :maxdepth: 1

   archives


.. _xivo-upgrade_script:

xivo-upgrade script
===================


.. note::
   * You can't use xivo-upgrade if you have not run the wizard yet
   * Upgrading from a version prior to *XiVO PBX* 1.2 is not supported.
   * When upgrading XiVO, you **must** also upgrade **all** associated XiVO
     Clients. There is currently no retro-compatibility on older *XiVO PBX* Client
     versions.

This script will update *XiVO PBX* and restart all services.

There are 2 options you can pass to xivo-upgrade:

* ``-d`` to only download packages without installing them. **This will still upgrade the package containing xivo-upgrade and xivo-service**.
* ``-f`` to force upgrade, without asking for user confirmation

``xivo-upgrade`` uses the following environment variables:

* ``XIVO_CONFD_PORT`` to set the port used to query the :ref:`HTTP API of xivo-confd <confd-api>`
  (default is 9486)


Troubleshooting
===============

Postgresql
----------

When upgrading XiVO, if you encounter problems related to the system locale, see
:ref:`postgresql_localization_errors`.


xivo-upgrade
------------

If xivo-upgrade fails or aborts in mid-process, the system might end up in a faulty condition. If in
doubt, run the following command to check the current state of xivo's firewall rules::

   iptables -nvL

If, among others, it displays something like the following line (notice the DROP and 5060)::

   0     0 DROP       udp  --  *      *       0.0.0.0/0            0.0.0.0/0           udp dpt:5060

Then your XiVO will not be able to register any SIP phones. In this case, you must delete the DROP
rules with the following command::

   iptables -D INPUT -p udp --dport 5060 -j DROP

Repeat this command until no more unwanted rules are left.


Upgrade Notes
=============

See :ref:`xivosolutions_release` for version specific informations.

Archives
--------

See :ref:`xivo_archive_release` for old release notes information.

