.. _upgrade_recording_xpbx:

**********************************
XiVOCC Recording upgrade procedure
**********************************

.. note:: Since 2017.03.02, ``xivo-recording`` and ``call-recording-filtering`` packages are deprecated and are replaced by package ``xivocc-recording``. This page describe the upgrade procedure
          (for :ref:`feature description, see here <recording>`).

``xivo-recording`` and ``call-recording-filtering`` packages are deprecated, but they were not uninstalled from your **XiVO PBX** during the upgrade.

You now have to follow this manual procedure:

.. note:: This has to be done on **XiVO PBX**

#. Install new ``xivocc-recording`` package::

    apt-get install xivocc-recording

#. Configure ``xivocc-recording`` package (when ask, **take care** to enter same IP for Recording server and same *XiVO PBX* name as in previous configuration)::

    xivocc-recording-config

#. **Update all the different locations** where the old ``xivo-incall-recording`` or ``xivo-outcall-recording`` subroutines
   were called and **change them** to now call the new ``xivocc-incall-recording`` or ``xivocc-outcall-recording`` subroutines:

   #. either in file :file:`/etc/xivo/asterisk/xivo_globals.conf`
   #. or in Custom dialplan

#. If you made specific recording subroutines you should also compare files :file:`/etc/asterisk/extensions_extra.d/xivo-recording.conf` and
   :file:`/etc/asterisk/extensions_extra.d/xivocc-recording.conf` and transfer all custom changes to the new ``xivocc-recording.conf``.

#. If there are some audio files in the failed directory of previous installation you should move them::
    
    mv /var/spool/xivo-recording/failed/*.wav /var/spool/xivocc-recording/failed/

#. When you're done test that recording still works. Test that files are recorded, correctly sent to Recording server (see
   ``/var/log/xivocc-recording/replication.log``), correctly displayed in the Recording server interface,
#. If it works correctly, you should remove deprecated packages with (**take care**, it will remove the package and all associated
   configuration files)::
   
    apt-get purge xivo-recording call-recording-filtering

