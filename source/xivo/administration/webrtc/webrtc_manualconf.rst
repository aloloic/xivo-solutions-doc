***************************
WebRTC manual configuration
***************************

.. note:: This is the manual way to configure a WebRTC line. It is here *for the record*. You should follow the :ref:`configure_user_with_webrtc_line` instead.

1. Create user

2. Optional: set codec to ulaw

.. figure:: webrtc_user_codecs.png
    :scale: 100%

3. Add line to user without any device

4. Configure Advanced Line options, so that it is usable with the softphone WebRTC

::

    avpf = yes
    call-limit = 1
    dtlsenable = yes ; Tell Asterisk to enable DTLS for this peer
    dtlsverify = no ; Tell Asterisk to not verify your DTLS certs
    dtlscertfile=/etc/asterisk/keys/asterisk.pem ; Tell Asterisk where your DTLS cert file is
    dtlsprivatekey = /etc/asterisk/keys/asterisk.pem ; Tell Asterisk where your DTLS private key is
    dtlssetup = actpass ; Tell Asterisk to use actpass SDP parameter when setting up DTLS
    encryption = yes
    force_avp = yes
    icesupport = yes
    transport = ws

.. figure:: webrtc_line_manual.png
    :scale: 100%
