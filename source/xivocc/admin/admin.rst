.. _admin:

**************
Administration
**************

.. _admin_version:

Show containers and images versions
-----------------------------------

.. note:: Introduced in 2017.03.03 release.

Docker images are labelled with the exact version of the embedded application.

You can display the:

* Version of the running docker containers by typing:

  .. code-block:: bash

    xivocc-dcomp version

* Version of all docker containers (including stopped ones) by typing:

  .. code-block:: bash

    xivocc-dcomp version -a

* Version of docker images:

  .. code-block:: bash

    xivocc-dcomp version -i

For example :

.. code-block:: sh

  # xivocc-dcomp version
  NAMES                       VERSION
  xivocc_nginx_1
  xivocc_xuc_1                2017.03.03
  xivocc_recording_server_1   2017.03.03
  xivocc_xucmgt_1             2017.03.03
  xivocc_config_mgt_1         2017.03.02
  xivocc_pack_reporting_1     2017.03.02
  xivocc_spagobi_1            2017.03.02
  xivocc_xivo_stats_1         2017.03.02
  xivocc_xivo_replic_1        2017.03.02
  xivocc_pgxivocc_1
  xivocc_elasticsearch_1
  xivocc_fingerboard_1
  xivocc_recording_rsync_1
  xivocc_kibana_volumes_1



This only applies to the following images:

* config_mgt
* pack_reporting
* recording
* spagobi
* xivo_replic
* xivo_stats
* xuc
* xucmgt

.. note:: Labels were in fact introduced in 2017.03.02 but in this version you had to use docker commands to display them:

   * List all running containers with the exact version of application

     .. code-block:: sh

               $ docker ps --format 'table {{.Names}}\t{{.Image}}\t{{.Label "version"}}\t{{.Status}}'
               NAMES                       IMAGE                                         VERSION             STATUS
               xivocc_spagobi_1            xivoxc/spagobi:2017.03.latest                 2017.03.02          Up 14 hours
               xivocc_nginx_1              xivoxc/xivoxc_nginx:latest                                        Up 32 hours
               xivocc_xuc_1                xivoxc/xuc:2017.03.latest                     2017.03.02          Up 13 hours
               xivocc_recording_server_1   xivoxc/recording-server:2017.03.latest        2017.03.02          Up 32 hours
               xivocc_xivo_replic_1        xivoxc/xivo-db-replication:2017.03.latest     2017.03.02          Up 32 hours
               xivocc_config_mgt_1         xivoxc/config-mgt:2017.03.latest              2017.03.02          Up 32 hours
               xivocc_pack_reporting_1     xivoxc/pack-reporting:2017.03.latest          2017.03.02          Up 32 hours
               xivocc_xivo_stats_1         xivoxc/xivo-full-stats:2017.03.latest         2017.03.02          Up 32 hours
               xivocc_pgxivocc_1           xivoxc/pgxivocc:latest                                            Up 32 hours
               xivocc_xucmgt_1             xivoxc/xucmgt:2017.03.latest                  2017.03.02          Up 32 hours
               xivocc_elasticsearch_1      elasticsearch:1.7.2                                               Up 32 hours
               xivocc_fingerboard_1        xivoxc/fingerboard:latest                                         Up 32 hours
               xivocc_recording_rsync_1    xivoxc/recording-rsync:latest                                     Up 32 hours
               xivocc_kibana_volumes_1     xivoxc/kibana_volume:latest                                       Up 32 hours

   * You can also inspect an image or container to get it's exact version:

     .. code-block:: sh

                # Inspect an image
                $ docker inspect --format '{{(index .Config.Labels "version")}}' xivoxc/xuc:2017.03.latest
                2017.03.02

                # Inspect a running container
                $ docker inspect --format '{{(index .Config.Labels "version")}}' xivocc_xuc_1
                2017.03.02


Log
---

The log of each container can be found in the `/var/log/xivocc` directory.
Currently (it may change) the structure looks like this :

.. code-block:: sh

    /var/log/xivocc:
    ├── config-mgt
    │   └── config-mgt.log
    ├── purge-reporting-database.log
    ├── recording-server
    │   ├── downloads.log
    │   └── recording-server.log
    ├── spagobi
    │   ├── Quartz.log
    │   ├── SpagoBIBirtReportEngine.log
    │   ├── SpagoBIChartEngine.log
    │   ├── SpagoBIJasperReports.log
    │   ├── SpagoBI.log
    │   ├── SpagoBIQbeEngineAudit.log
    │   ├── SpagoBIQbeEngine.log
    │   └── SpagoBITalendEngine.log
    ├── specific-stats.log
    ├── xivo-db-replication
    │   └── xivo-db-replication.log
    ├── xivo-full-stats
    │   └── xivo-full-stats.log
    ├── xuc
    │   ├── xuc_ami.log
    │   └── xuc.log
    └── xucmgt
        └── xucmgt.log


Backup
------

You may backup your statistic database by using a similar command as below

.. code-block:: ini

    docker run --rm --link demo_pgxivocc_1:db -v $(pwd):/backup -e PGPASSWORD=xivocc postgres pg_dump -h db -U postgres --format=c -f /backup/xivo_stats_dump xivo_stats


Restore
-------

You may restore a backup using a similar command (to be adapted)

.. code-block:: ini

    docker run --rm -it --link pgxivoccdemo_pgxivocc_1:db -v $(pwd):/backup postgres pg_restore -h db -c -U postgres -d xivo_stats /backup/xivo_stats_dump
