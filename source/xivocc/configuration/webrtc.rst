********************
WebRTC configuration
********************

See :ref:`Web RTC feature description <webrtc_requirements>`.

.. _webrtc-ssl-cert:

Signed SSL/TLS certificate for WebRTC
=====================================

XivoCC installation generates self-signed SSL/TLS into nginx server running as part of XivoCC. This limits WebRtc usage:

* :ref:`web-assistant` shows warning about unsecure page and exception must be confirmed by user.
* :ref:`desktop-assistant` must be started with ``--ignore-certificate-errors`` parameter, which degrades security.

To avoid this, SSL/TLS certificate signed by authority recognized by Chrome in PEM format is required, see :ref:`nginx-trusted-certificate`.
