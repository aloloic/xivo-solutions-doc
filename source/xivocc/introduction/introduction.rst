************
Introduction
************

Xivo-CC provides enhancements of the XiVO PBX contact center functionalities.
It gives especially acces to outsourced statistics, real-time supervision screens,
third-party CTI integration and recording facilities.