.. _xci_configuration_of_centralized_routing:

**********************************************
Configuration of Xivos for centralized routing
**********************************************

When you add Xivo with configuration, basic Xivo configuration is done automatically. However there are still
some steps which have to be done manually on each Xivo using Xivo WebUI.

------------------------------------------
Xivo configuration for centralized routing
------------------------------------------

Things to verify on Xivo before manual configuration
====================================================

Verify that:

1. Xivo was added to XCI with configuration.
2. Xivo was restarted (automatically when added do XCI or manually later).
3. You have know routing context name for each xivo in form ``to_xxx`` - it is configured in XCI when adding Xivo.
4. Routing script ``/usr/share/asterisk/agi-bin/xivo_routage_agi.py`` is there, executable by asterisk user.
5. Dialplan routing configuration ``/etc/asterisk/extensions_extra.d/routage.conf`` is there, readable by asterisk user.

Manually update address of routing server
=========================================

There must be a configuration file ``/etc/xivo_routage.conf`` with ``hosts = IP:9000`` where IP is the IP address of XCI
server. If you have a backup routing server, there can be multiple IP:PORT couples separated by comma. Example:

.. code-block:: ini

    [general]
    hosts = 192.168.111.222:9000,192.168.111.333:9000

Connect Xivos by trunks
=======================

Ensure there is (direct or indirect) trunk connection between every two Xivos -
see :ref:`interconnect_two_xivos_directly`. This trunks must have Context set to ``Incall``.

Add routing contexts
====================

In each Xivo create routing contexts for every other Xivo. In Xivo WebUI go to: Services / IPBX / IPBX configuration
/ Contexts and click Plus icon. For example: if you have three Xivos A, B, C with routing contexts ``to_xivo_a``,
``to_xivo_b``, ``to_xivo_c``, then in Xivo B you create routing contexts ``to_xivo_a`` and ``to_xivo_c``. Type of
routing context must be Outcall and it must not include any sub-contexts.


Add outgoing calls
==================

For each routing context in every Xivo you need to add Outgoing call. In Xivo WebUI go to: Services / IPBX / Call
management / Outgoing calls and click Plus icon. This outgoing call has:

* usually the same name as respective routing context
* Context set to respective routing context
* Trunk set to trunk connecting (directly or indirectly) to target Xivo
* in Exten tab single line with Exten = ``X.`` and Stripnum = ``0``

-------------------------
Debug centralized routing
-------------------------


On each Xivo you can check log requests and result for routing requests by command:

.. code-block:: bash

    tail -F /var/log/asterisk/xivo-routage-agi.log


On XCI you can check log requests and result for routing requests by command:

.. code-block:: bash

    docker logs -f icdu_routing_server_1

