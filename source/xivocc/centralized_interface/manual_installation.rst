.. _gcu_manual_installation:

*******************
Manual installation
*******************

.. warning:: Manual installation steps are provided for debugging purposes and for special cases - for common cases please follow :ref:`Installation <gcu_installation>`.

The configuration files and the Docker-Compose files are available in a specific `Git repository <https://gitlab.com/xivo-utils/icdu-packaging>`_.

Database setup
--------------

XCI stores some data in a PostgreSQL database. By default, ``application.conf`` is configured to connect to a local database named ``icx`` with the username ``icx`` and password ``icx``. You can change these parameters if you wish. We will use the default parameters in this documentation.

First, we need to install PostgresSQL extensions to use UUID functions :

.. code-block:: bash

	sudo apt-get install postgresql-contrib

We can now create the user and the database associated :

.. code-block:: bash

	sudo -u postgres psql -c "CREATE USER icx WITH PASSWORD 'icx'"

.. code-block:: bash

	sudo -u postgres psql -c "CREATE DATABASE icx WITH OWNER icx"

We then have to enable UUID extension on the ``icx`` database. Connect as ``root`` on the ``icx`` database :

.. code-block:: bash

	sudo -u postgres psql icx -c 'CREATE EXTENSION IF NOT EXISTS "uuid-ossp";'

I can't connect to PostgreSQL
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is possible that PostgreSQL complains when you're trying to connect. The solution is to modify the ``pg_hba.conf`` (in Debian, located in ``/etc/postgresql/X.X/main``) and add the following line at the end :

.. code-block:: bash

	local	all	all	trust

Generate SSH key
----------------

In order to let XCI communicate with the various XiVOs, an SSH key is used. Generate one using the following command :

.. code-block:: bash

	ssh-keygen -t rsa -f /etc/docker/interface-centralisee/ssh_key

