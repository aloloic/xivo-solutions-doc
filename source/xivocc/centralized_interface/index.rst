***************************
XiVO Centralized Interface
***************************

The XiVO Centralized Interface (XCI) allows to manage several XiVO servers through a unique web interface. Thanks to this interface, it becomes possible to quickly add users that are automatically routed across servers. This documentation will describe the installation process of the interface, how to use the web interface and the REST API it exposes.

.. toctree::
	:maxdepth: 2

	intended_usage
	installation
	centralized_routing_configuration
	manual_installation
	interface
	api

