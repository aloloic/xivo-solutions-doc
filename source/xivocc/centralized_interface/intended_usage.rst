.. _xci_intended_usage:

**************
Intended usage
**************

The XiVO Centralized Interface (XCI) is intended for multi-Xivo systems with centralized routing and user management.

.. warning::

        Using XCI for other use-cases than the one described bellow is neither supported nor recommended.
        If XCI routing schema or centralized user management does not fit your use case, XCI is not good solution for your
        telephony system. Using XCI for user management only is not recommended.
        Non-standard installations may be broken by update to future version without warning.

-------
Routing
-------

Routing supports heterogeneous numbering plan administration:

* Centralized dialplan management.
* Route incoming and outgoing calls, independently of the entry point to the target telephony subsystem hosting this number.
* Simple configuration of dialplan richness (prefix, short numbers, numbers of different length, emergency calls, live destination modification).
* Easily configurable protection against routing loops.

Routing is using concept of two layers:

1. "Logical" layer - based on contexts, users are routed using centralized database returning the context to be used
   to reach the user, with extensions correctly routed irrespective to point of entry of call.
2. "Physical" layer - arbitrary connection (direct or indirect) via trunks is supported. Trunks are associated
   to contexts to reflect the real network topology, there's no need of full-mesh topology, a call can pass by
   multiple Xivos before reaching the user.

Mapping between "Logical" and "Physical" layer is done by routing contexts. Every Xivo has routing context for every
other Xivo (attached to trunk it will use). Routing context with the same name on different Xivo will be configured
accordingly to the position of the Xivo in the network. Intervals can overlap between Xivos, therefore logical
structure of dialplan can be independent on physical location of users.

.. image:: diagrams/XCI_routing.png
	:align: center

See :ref:`xci_configuration_of_centralized_routing` for more configuration details.

Implementation of routing
=========================

When there is dial request on some Xivo and destination number is not found locally, AGI script in the dialplan requests
a route from the routing database and gets:

* A context used to process the call.
* Rules to update the destination number.
* Requests can be chained.


.. note::

        Routing systems provides also the conversion of the direct incoming number to the user's short number,
        this conversion is done when the call enters the system, between Xivos only the short number is used.
        This condition needs to be respected when creating a new system or integrating an existing one. When integrating
        an existing one, you may need to create some routes manually.

Fault tolerance:

* Local calls work, even when other network links and/or centralized user database are unreachable.
* Routing server can be setup in High Availability master-slave mode. When master routing server is down, routing
  requests are automatically processed by the slave.

---------------------------
Centralized user management
---------------------------

User management allows centralized administration without knowledge of low-level telephony details:

* Configuration is done from point of view of organization administrator, not telephony technician.
* User creation is simplified by line templates.
* Only relevant choices and options are presented.
* Extension number for new user is checked to be unique among all Xivos.
* Numbers proposed when creating a user are based on their availability, the number not used since the longest time will
  be reassigned first.
* XCI accounts can be restricted to manage only part of the system.
* All configuration changes are logged for auditability.

Compatability with configuration via Xivo WebUI:

* Users added by Xivo WebUI before adding Xivo to XCI are imported, but they not reachable by centralized routing.
* Combining user management by XCI and by Xivo WebUI is bad idea, which usually leads to misconfiguration.
* Managing users by XCI and call-center configuration (Queues, Agents) via Xivo WebUI is possible, but queue numbers
  are not reachable by centralized routing.
* Configuring conference rooms via Xivo WebUI is possible, but conference room numbers are not reachable by centralized
  routing.
