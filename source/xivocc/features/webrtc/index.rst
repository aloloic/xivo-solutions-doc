.. _webrtc_requirements:

******************
WebRTC Environment
******************

.. note:: added in version 2016.04

From version 2016.04 one can use WebRTC with *XiVO PBX* and *XiVO CC* in the following environment:

* LAN network (currently no support for WAN environment),
* with the:

  * *Web Assistant* with Chrome browser:

    * version 55 (tested on 55.0.2883.87 m 64-bit) with 2016.04 or later,
    * version 57 requires XiVO-CC version 2017.02 or later

  * or *Desktop Assistant*

Requirements
============

The **requirements** are:

* to have a microphone and headphones for your PC,
* to configure your *XiVO PBX*:

  * open WebRTC access (see::ref:`configure_xpbx_webrtc`) taking *great care of the security notice*,
  * and then create users with a WebRTC line (see: :ref:`configure_user_with_webrtc_line`),

* have a SSL/TLS certificate signed by a certification authority installed on the nginx of *XiVO CC* (see: :ref:`webrtc-ssl-cert`),
* and use *https*:

  * *Web Assistant*: you must connect to the *Web Assistant* via `https` protocol,
  * *Desktop Application*: you must check *Protocol -> Secure* in the application parameters.

.. note:: Currently you can not have a user configured for both WebRTC and a phone set at the same time.

Limitations
===========

Known limitation are :

* Voice may not be able to hear if your computer have more than 4 network interfaces up at the same time (this can happen if you use virtualization)

.. note::

  | To check if you have more than 4 network interfaces you can type following command:
  | ``ls /sys/class/net``

  | Then just use:
  | ``ifdown <ifname>``

  | This will switch off network interface not required to make your call.
