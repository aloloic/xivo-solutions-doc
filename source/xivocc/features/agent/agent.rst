.. _agent:

#################
Agent environment
#################

.. figure:: agent.png
    :scale: 80%

Web application for contact center agents. Some parameters for Recording, Callbacks, Queue control, Pause statuses and Sheet popup may be configured. Instructions are to be found in the :ref:`configuration section <agent_configuration>`.
The application offers a basic support for the WebRTC lines, currently there's a limitation on complementary services
like conference or second call which are not supported.

=====
Login
=====

.. figure:: agent-login.png
    :scale: 80%

Enter your XiVO client username, password and phone set you want to logged to on the login page.

If you are using Kerberos authentication and enabled SSO (see :ref:`kerberos-configuration`), then you only have to set your phone set number, the authentication and login will be done automatically:

.. figure:: agent-login-sso.png
    :scale: 80%


============
Call control
============

Call can be controlled through button inside the Agent interface where you can

- Answer
- hangup
- Hold / Unhold : The color of the button indicates the possible action (grey: no action, green: put on hold, red: unhold)
- Attended transfer
- Complete transfer
- Cancel transfer
- Conference

.. figure:: agent-call-control.png
    :scale: 90%

The Agent interface use Desktop notification but this feature needs to be enabled from the browser window when logging in:

.. figure:: agent-notification.png
    :scale: 90%

.. note:: The transfer support is limited, to transfer a call you need to write the number to the search field, then click
          the attended transfer button and then you can complete the transfer by the complete transfer button.

.. note:: Conference and second call except the attended transfer are not supported for agents with webRTC lines.

================
Taking Callbacks
================

The agent can see the :ref:`callbacks <callbacks>` related to the queues he is logged on.
They are available in the ``Callbacks`` tab, beside the ``Agents of my group`` tab.

On this page, the agent only has access to basic information about the callback: the phone number to call,
the person's name and its company name. On the left of each callback line, a colored clock indicates the temporal status of this callback:

- yellow if the callback is to be processed later
- green if we are currently inside the callback period
- red if the callback period is over

.. figure:: agent-callbacks-view.png
    :scale: 90%

To process one of these callbacks, the agent must click on one of the callbacks line. This will remove the callback from the other agents' list,
and trigger the following screen:

.. figure:: agent-callback-edit.png
    :scale: 90%

To launch the call, the agent must click on one of the available phone numbers.
Once the callback is launched, the status can be changed and a comment can be added.

If you set 'Callback' as status, the callback can be rescheduled at a later time and another period:

.. figure:: agent-callback-reschedule.png
    :scale: 90%

Clicking on the calendar icon next to the "New due date" field, will popup a calendar to select another callback date.

Every hour a desktop notification is shown to display pending callbacks that are about to expire. Please note that the display may vary depending on operating system and browser.

.. figure:: agent-callback-notification.png
    :scale: 80%

