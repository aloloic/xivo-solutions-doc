*******
Upgrade
*******

Overview
========

The following components will be upgraded :

- Docker images
- xivocc-installer package

.. warning:: This upgrade procedure applies only to XiVO CC installed via the ``xivocc-installer`` package.


Before upgrading you have to check or change your sources list.
It should be located in the file :file:`/etc/apt/sources.list.d/xivo-solutions.list`.

There are three cases:

#. :ref:`Upgrade to the latest XiVO CC version <upgrade_latest_version_xcc>`,
#. :ref:`Upgrade to a specific version <upgrade_specific_version_xcc>` (or archive version),
#. :ref:`Upgrade to the latest subversion <upgrade_latest_subversion_xcc>` of your current installed version.


.. _upgrade_latest_version_xcc:

Upgrade to **latest** version
-----------------------------

To upgrade to the latest version the sources list must point towards *debian* URI and *xivo-solutions* suite::

    deb http://mirror.xivo.solutions/debian/ xivo-solutions main


.. _upgrade_specific_version_xcc:

Upgrade to **specific** version
-------------------------------

To upgrade to a **specific** version the sources list must point towards *archive* URI and *xivo-solutions-VERSION-latest* suite.

For example if you want to upgrade to **2017.03** version you should have::

    deb http://mirror.xivo.solutions/archive/ xivo-solutions-2017.03-latest main

Note the ``/archive/`` and ``-2017.03-latest`` above.


.. _upgrade_latest_subversion_xcc:

Upgrade to latest **subversion**
--------------------------------

After the release of a *version* (e.g. *2017.03*) we may backport some bugfixes in this version.
We will then create a **subversion** (e.g. 2017.03 **.01**) shipping these bugfixes.
To upgrade to the **latest subversion** of your current installed *version* you need to:

*XiVO CC >= 2017.03* :

#. Verify that the :file:`/etc/docker/compose/factory.env` file has

   #. ``XIVOCC_TAG=VERSION`` (where ``VERSION`` is your current installed *version* - e.g. *2017.03*)
   #. and ``XIVOCC_DIST=latest``
#. Then update the images with ``xivocc-dcomp pull`` command,
#. And upgrade the containers with ``xivocc-dcomp up -d`` command.

*XiVO CC >= 2016.04* :

#. Verify that the :file:`/etc/docker/compose/.env` file has

   #. ``XIVOCC_TAG=VERSION`` (where ``VERSION`` is your current installed *version* - e.g. *2016.04*)
   #. and ``XIVOCC_DIST=latest``
#. Then update the images with ``dcomp pull`` command,
#. And upgrade the containers with ``dcomp up -d`` command.

*XiVO CC = 2016.03* :

#. Verify that the file :file:`/etc/docker/compose/docker-compose.yml` contains the tag *2016.03* **.latest** for the appropriate containers,
#. Then update the images with ``dcomp pull`` command,
#. And upgrade the containers with ``dcomp up -d`` command.


Preparing the upgrade
=====================

To prepare the upgrade you should:

#. Read :ref:`xivosolutions_release` starting from your version to the version you target.


Upgrade
=======

When you have checked the sources.list you can upgrade with the following commands::

    apt-get update
    apt-get install xivocc-installer

The current ``docker-compose.yml`` file will be renamed to ``docker-compose.yml.dpkg-old`` and new template downloaded.
A new ``docker-compose.yml`` file will be rendered from the template using the current xivocc version.

Then you have to:

#. Download the new images::

    xivocc-dcomp pull

#. And run the new container (**All XiVO CC services will be restarted**)::

    xivocc-dcomp up -d

.. note::
   Please, ensure your server date is correct before starting. If system date differs too much from correct date, you may get an authentication error preventing download of the docker images.

Post Upgrade
============

Check your upgrade through :ref:`check-list`.

Upgrade notes
=============

See :ref:`xivosolutions_release` for version specific informations.
