*****************
Phone integration
*****************

XUC based web applications like agent interface or xivo client web integrates buttons for phone control. This section
details necessary configuration, supported phones and limitations.

Note: The voip vlan network have to be accessible by the xivocc xuc server

.. _phone_integration_support:

Supported phones
================

+-----------------+---------------------------------------------------------------------------+
| Manufacturer    | Function                                                                  |
|                 +--------+--------+------+------------+-------------------+-----------------+
|                 | Answer | Hangup | Hold | Conference | Attended Transfer | Direct Transfer |
+=================+========+========+======+============+===================+=================+
| Snom 7XX        |   OK   |   OK   |  OK  |     OK     |        OK         |       OK        |
+-----------------+--------+--------+------+------------+-------------------+-----------------+
| Polycom VVX     |   OK   |   OK   |  OK  |     NO     |        OK         |       OK        |
+-----------------+--------+--------+------+------------+-------------------+-----------------+
| Yealink T4X     |   OK   |   OK   |  OK  |     NO     |        OK         |       OK        |
+-----------------+--------+--------+------+------------+-------------------+-----------------+

* NO - Not available

.. _phone_integration_installation:

Required configuration
======================

The following steps are not required if you updated the Provisioning plugins.

Customize templates for Polycom phones
--------------------------------------

.. warning::
   Only required for plugins:
   
   - xivo-polycom-4.0.9 version below v1.9
   - xivo-polycom-5.4.3 version below v1.8
     

To enable phone control buttons on web interfaces you must update the basic template of Polycom phones:

- go to the plugin directory: `/var/lib/xivo-provd/plugins/xivo-polycom-VERSION`
- copy the default template from `templates/base.tpl` to `var/templates/`
- then you must update `app.push` parameters in the else section **(do not replace switchboard settings)** as follows:

.. code-block:: ini

    apps.push.messageType="5"
    apps.push.username="guest"
    apps.push.password="guest"

Customize templates for Yealink phones
--------------------------------------

.. warning::
   Only required for plugins xivo-yealink-v80 below v1.31
   
To enable phone control buttons on web interfaces you must update the basic template of Yealink phones:

- go to the plugin directory: `/var/lib/xivo-provd/plugins/xivo-yealink-VERSION`
- copy the default template from `templates/base.tpl` to `var/templates/`
- enable sip notify even for non switchboard profiles **(do not replace switchboard settings)**

.. code-block:: ini

        {% if XX_options['switchboard'] -%}
        push_xml.sip_notify = 1
        call_waiting.enable = 0
        {% else -%}
        push_xml.sip_notify = 1
        call_waiting.enable = 1
        {% endif %}


Update Device Configuration
---------------------------
- to update device configuration you must run :command:`xivo-provd-cli -c 'devices.using_plugin("xivo-polycom-VERSION").reconfigure()'`
- and finally you must resynchronize the device: :command:`xivo-provd-cli -c 'devices.using_plugin("xivo-polycom-VERSION").synchronize()'`
- refer to provisioning_ documentation for more details
- if the phone synchronization fails check if the phone uses the version of the plugin you have updated, you can use
  :command:`xivo-provd-cli -c 'devices.find()'`

.. _provisioning : http://documentation.xivo.fr/en/stable/administration/provisioning/adv_configuration.html

