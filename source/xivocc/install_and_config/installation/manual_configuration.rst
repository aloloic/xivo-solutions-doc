.. _manual_configuration:

*************************************
Manual configuration and installation
*************************************

This section describes the manual installation of the XiVO CC components.
In most cases you **SHOULD NOT** follow this page, and install the XiVO CC components via the `xivocc-installer` package (see
:ref:`ccinstallation`).

.. note:: We leave this page here :

  - as a reference,
  - and when one must install only a subset of the XiVO CC components (since it is not currently possible 
    via the `xivocc-installer` package).


Prerequisites
=============

We will assume your *XiVO CC* server meets the following requirements:

- OS : Debian 8 (jessie), 64 bit
- the latest stable version of Docker_ is installed
- the latest stable version of Docker-compose_ is installed
- the XiVO PBX is reachable on the network
- the XiVO PBX is setup with users, queues and agents, you must be able to place and answer calls.

.. _Docker: https://www.docker.com
.. _Docker-compose: https://docs.docker.com/compose/install/

Note : Install only stable version of docker and docker compose.

We will make the following assumptions :

- the *XiVO PBX* has the IP 192.168.0.1
- some data (incoming calls, internal calls etc.) might be available on XiVO (otherwise, you will not see `anything` in the :ref:`check-list`).
- the *XiVO CC* server has the IP 192.168.0.2

.. check-list: installation#check-list

XiVO PBX configuration
======================

PostgreSQL configuration
------------------------

Firstly, allow access to PostgreSQL from the outside. Edit :file:`/etc/postgresql/9.4/main/postgresql.conf`:

.. code-block:: ini

   listen_addresses = '*'

Add this line to :file:`/etc/postgresql/9.4/main/pg_hba.conf`:

.. code-block:: ini

   host asterisk all 192.168.0.2/32 md5

Create a user `stats` with read permissions :

.. code-block:: bash

   sudo -u postgres psql asterisk << EOF
   CREATE USER stats WITH PASSWORD 'stats';
   GRANT SELECT ON ALL TABLES IN SCHEMA PUBLIC TO stats;
   EOF

And run ``xivo-service restart all`` to apply these modifications.

AMI configuration
-----------------

* Add file :file:`/etc/asterisk/manager.d/02-xivocc.conf` directory with the following
  content, replacing *X.X.X.X* by your xucserver IP address :

.. code-block:: ini
   :emphasize-lines: 4

   [xuc]
   secret = xucpass
   deny=0.0.0.0/0.0.0.0
   permit=X.X.X.X/255.255.255.255
   read = system,call,log,verbose,command,agent,user,dtmf,originate,dialplan
   write = system,call,log,verbose,command,agent,user,dtmf,originate,dialplan

* And reload the AMI :

.. code-block:: bash

   asterisk -rx "manager reload"
   asterisk -rx "manager show user xuc" and check your if previous configuration is displayed.

CEL Configuration
-----------------

* Replace content of file :file:`/etc/asterisk/cel.conf` by the following :

.. code-block:: ini

   [general]
   enable = yes
   apps = dial,park,queue
   events = APP_START,CHAN_START,CHAN_END,ANSWER,HANGUP,BRIDGE_ENTER,BRIDGE_EXIT,USER_DEFINED,LINKEDID_END,HOLD,UNHOLD,BLINDTRANSFER,ATTENDEDTRANSFER

   [manager]
   enabled = yes

* and reload the cel module in Asterisk :

.. code-block:: bash

   asterisk -rx "module reload cel"

Customizations in the web interface
-----------------------------------

* Create a user *xuc* in :menuselection:`Services -> IPBX -> Users` with the following parameters:
 
 - CTI login : xuc
 - CTI password : 0000
 - profil supervisor

* Create a Web Services user in :menuselection:`Configuration -> Web Services Access` with the following parameters :

 - Login : xivows
 - Password : xivows
 - Host : 192.168.0.2

Make sure **Multiqueues call stats sharing** is enabled in :menuselection:`Services -> IPBX -> Advanced configuration` tab.

Phone integration
-----------------
Do not forget to follow configuration steps detailed in :ref:`Required configuration for phone integration
<phone_integration_installation>`.

Packages for the recording
--------------------------

Still on the *XiVO PBX*, install the package which will handle the recording.

See :ref:`Recording paragraph <recording_xpbx>` in installation section.


XiVO CC configuration
=====================

Now we switch to the installation of the XiVO CC server.

Install ntp server
------------------
.. code-block:: ini

    apt-get install ntp

*XiVO CC* server and *XiVO  PBX* server must be synchronized to the same source.

Enable Docker LogRotate
-----------------------

Docker container log output to /dev/stdout and /dev/stderr.
The Docker container log file is saved in /var/lib/docker/containers/[CONTAINER ID]/[CONTAINER_ID]-json.log.

Create a new Logrotate config file for your Docker containers in the Logrotate folder /etc/logrotate.d/docker-container.

.. code-block:: ini

        /var/lib/docker/containers/*/*.log {
          rotate 7
          daily
          compress
          missingok
          delaycompress
          copytruncate
        }

You can test it with logrotate -fv /etc/logrotate.d/docker-container.
You should get some output and a new log file with suffix [CONTAINER ID]-json.log.1 should be created.
This file is compressed in next rotation cycle.


Retrieve the configuration script and launch it:

Containers installation
-----------------------

.. code-block:: bash

   wget  https://gitlab.com/xivoxc/packaging/raw/master/install/install-docker-xivocc.sh
   bash install-docker-xivocc.sh

During the installation, you will be asked for :

- the XiVO IP address (e.g. 192.168.0.1)
- the number of weeks to keep for the statistics
- the number of weeks to keep for the recording files
- the external IP of the machine (i.e. the adress used afterwards for http URLs)

Create the following alias in your .bashrc file:

.. code-block:: bash

    vi ~/.bashrc
    alias dcomp='docker-compose -p xivocc -f /etc/docker/compose/docker-xivocc.yml'

Containers modification
-----------------------

The yml file :file:`/etc/docker/compose/docker-xivocc.yml` should have the correct tag version for each imeage.

Check also that the **XIVO_CTI_VERSION** is correct for the xuc container.

.. code-block:: yaml

    xivo_replic :
      image: xivoxc/xivo-db-replication:2016.03.latest
     
    xivo_stats :
      image: xivoxc/xivo-full-stats:2016.03.latest
    
    pack_reporting:
      image: xivoxc/pack-reporting:2016.03.latest
    
    config_mgt:
      image: xivoxc/config-mgt:2016.03.latest
    
    recording_server:
      image: xivoxc/recording-server:2016.03.latest
    
    xuc:
      image: xivoxc/xuc:2016.03.latest

      environment:
      - XIVO_CTI_VERSION=2.1
    
    xucmgt:
      image: xivoxc/xucmgt:2016.03.latest


Starting XivoCC
---------------

Then you can launch the XiVO CC with the following command :

.. code-block:: bash

   dcomp up -d


List XivoCC services :


.. code-block:: bash


    # dcomp ps
               Name                         Command               State                        Ports
    ---------------------------------------------------------------------------------------------------------------------
    xivocc_config_mgt_1         bin/config-mgt-docker            Up       0.0.0.0:9100->9000/tcp
    xivocc_elasticsearch_1      /docker-entrypoint.sh elas ...   Up       0.0.0.0:9200->9200/tcp, 0.0.0.0:9300->9300/tcp
    xivocc_fingerboard_1        /bin/sh -c /usr/bin/tail - ...   Up
    xivocc_kibana_volumes_1     /bin/sh -c /usr/bin/tail - ...   Up
    xivocc_nginx_1              nginx -g daemon off;             Up       443/tcp, 0.0.0.0:80->80/tcp
    xivocc_pack_reporting_1     /bin/sh -c echo            ...   Up
    xivocc_pgxivocc_1           /docker-entrypoint.sh postgres   Up       0.0.0.0:5443->5432/tcp
    xivocc_postgresvols_1       /bin/bash                        Exit 0
    xivocc_recording_server_1   bin/recording-server-docker      Up       0.0.0.0:9400->9000/tcp
    xivocc_reporting_rsync_1    /usr/local/sbin/run-rsync.sh     Up       0.0.0.0:873->873/tcp
    xivocc_spagobi_1            /bin/sh -c /root/start.sh        Up       0.0.0.0:9500->8080/tcp
    xivocc_timezone_1           /bin/bash                        Exit 0
    xivocc_xivo_replic_1        /usr/local/bin/start.sh /o ...   Up
    xivocc_xivo_stats_1         /usr/local/bin/start.sh /o ...   Up
    xivocc_xivocclogs_1         /bin/bash                        Exit 0
    xivocc_xuc_1                bin/xuc_docker                   Up       0.0.0.0:8090->9000/tcp
    xivocc_xucmgt_1             bin/xucmgt_docker                Up       0.0.0.0:8070->9000/tcp
