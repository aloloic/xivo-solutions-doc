*******************************
XiVO Five Intermediate Versions
*******************************

2017.02
=======

Consult the `2017.02 Roadmap <https://projects.xivo.solutions/versions/12>`_

+-----------------------------+----------------+
| Component                   | latest ver.    |
+=============================+================+
| XiVO PBX                    | 2017.02        |
+-----------------------------+----------------+
| xivoxc/xivoxc_nginx         | 0.8            |
+-----------------------------+----------------+
| xivoxc/xucmgt               | 2017.02.02     |
+-----------------------------+----------------+
| xivoxc/xuc                  | 2017.02.00     |
+-----------------------------+----------------+
| xivoxc/recording-server     | 2017.02.00     |
+-----------------------------+----------------+
| xivoxc/spagobi              | 2017.02.latest |
+-----------------------------+----------------+
| xivoxc/config-mgt           | 2017.02.00     |
+-----------------------------+----------------+
| xivoxc/pack-reporting       | 2017.02.00     |
+-----------------------------+----------------+
| xivoxc/xivo-full-stats      | 2017.02.00     |
+-----------------------------+----------------+
| xivoxc/xivo-db-replication  | 2017.02.00     |
+-----------------------------+----------------+
| xivoxc/pgxivocc             | 1.3            |
+-----------------------------+----------------+
| elasticsearch               | 1.7.2          |
+-----------------------------+----------------+
| xivoxc/fingerboard          | 0.6            |
+-----------------------------+----------------+
| xivoxc/kibana_volume        | 0.1            |
+-----------------------------+----------------+
| xivoxc/recording-rsync      | 1.0            |
+-----------------------------+----------------+

**Web Assistant**

* Fix WebRTC implementation compatibility with Chrome v57 and later, see :ref:`webrtc_requirements` for details.
* Fixes and enhancements of error handling while using WebRTC.
* Fixed problem when starting second call while using using SIP phone.

**CCAgent**

* Realtime statistics for inbound calls now take into account **only** ACD calls (it doesn't take anymore internal calls).
* Configuration of :ref:`agent login/pause management using phone function keys <login_pause_funckeys>` was simplified and documented.

**CCManager**

* Added ACD version of Agent statistics of inbound calls. New users have column version of statistics (incl. non-ACD calls)
  hidden by default, but can be added in column selection.
* In Global View when Compact view option is set, agents are displayed on a single line with firstname only and lines without agents are hidden.
* Added new column in Agent View to display if base configuration matches the active configuration (see :ref:`ccmanager_base_configuration_filter`).

**XiVO PBX**

* The subroutines shipped in the package ``xivo-recording`` (see: :ref:`recording_xpbx`) were updated to include the possibility to start recording in pause.
  **Take care to update your installation accordingly.**
* The *Default french configuration* option of the :ref:`Wizard <configuration_wizard>` now configures also 'Default config device' (see :ref:`default_french_conf`).

**Reporting**

* SpagoBI sample reports have been revamped for better consistency and readability. Added profit-sharing + agent activity per week reports.
  To upload these new *sample reports* see :ref:`spagobi`.

  .. note::
    * The new sample reports will be uploaded in a new *Samples* directory.
    * It will not overwrite the old sample report (located under *Standards* and *System* directories).
    * You may want to remove manually the old sample reports.

**System**

* When you add a new user with its line via *XiVO PBX* web interface, it is now visible in *XiVO CC* (CCAgent, CCManager) **without xuc server restart**.
  Limitation: when you are creating a new user, you **must** add the User and its line in the same step.


2017.01
=======

2017.01.01
----------

.. note:: Bugfix release. See :ref:`2017.01.00 section <2017_01_00_release>` for features list and behavior changes.

Consult the `2017.01.01 Roadmap <https://projects.xivo.solutions/versions/34>`_ for the list of fixes shipped in this version.

Bugfix highlight:

* WebAssistant - Cannot login with user account without provisioned device (like DECT or analog device). Temporary solution is to disable WEB RTC feature in xucmgt (see :ref:`webassistant_disable_webrtc`).
* SpagoBI - Fix default url parameter so the container use the host address instead of localhost.

.. _2017_01_00_release:

2017.01.00
----------

Consult the `2017.01 Roadmap <https://projects.xivo.solutions/versions/11>`_

**CCManager**

* CCManager Access management: The CCManager access will be enforced in a near future and will require a user with Supervisor or Administrator rights (defined in the Configuration Management tool).

  .. note::
    * In the meantime, when logging in the CCManager you may get a warning if you user account has no specific right defined
      (Message: "Warning, your user account has no profile defined. Please contact your administrator to create one.").
    * To prevent Agents from accessing the CCManager, you can enforce now the future behavior with a configuration parameter, see :ref:`Enforcing security in CCManager <ccmanager-security>`.

* New hamburger menu is available with option to maximize the different queue views. See :ref:`ccmanager`.
* Queue and Agent views have now fixed table header if size is bigger than single viewport.

**CCAgent**

* Hold/Unhold button is now colorized with proper call on hold status.

**XiVO PBX**

* The :ref:`Wizard <configuration_wizard>` has now a new option *Default french configuration*. This will import a predefined configuration enhanced for french *XiVO PBX* installations (see :ref:`default_french_conf`).
* The asterisk 13.13.1 version is available for upgrade with a specific procedure. See :ref:`upgrade_asterisk_latest`. This version will become the new default asterisk version in a future release but, currently,
  is only available with a specific upgrade procedure.


2016.04
=======

.. warning:: A **Security Issue** related to WebRTC activation in *XiVO PBX* was found in 2016.04.01.

    All *XiVO PBX* installations must be upgraded to latest 2016.04 (see :ref:`XiVO PBX Upgrade notes <upgrade>`).

    See http://mirror.xivo.solutions/security/XIVO-2017-01.pdf


2016.04.01
----------

.. note:: Bugfix release. See :ref:`2016.04.00 section <2016_04_00_release>` for features list and behavior changes.

Consult the `2016.04.01 Roadmap <https://projects.xivo.solutions/versions/25>`_ for the list of fixes shipped in this version.

Bugfix highlight:

* Cannot remotely logout agent from CCManager/Xivo
* Call establishment can take a long time because of the webRTC options


To upgrade, see :ref:`Upgrade to latest subversion <upgrade_latest_subversion_xcc>`.


.. _2016_04_00_release:

2016.04.00
----------

Consult the `2016.04 Roadmap <https://projects.xivo.solutions/versions/8>`_

**System**

* Parameters for :file:`/etc/docker/compose/docker-xivocc.yml` are now stored in :file:`/etc/docker/compose/.env` file. Important
  parameter is ``XIVO_AMI_SECRET``, which holds Ami password.
* To be able to use the :file:`/etc/docker/compose/.env` file, a new ``dcomp`` alias is generated in .bashrc. You must run::

   source .bashrc

  before running ``dcomp`` again.

.. note:: If you are using ``docker-compose`` instead of recommended alias ``dcomp``, make sure your current directory
 is ``/etc/docker/compose``, otherwise ``/etc/docker/compose/.env`` won't be used. i.e.::

  cd /etc/docker/compose
  docker-compose ...

**Web/Desktop Assistant**

* For displaying search result, compatibility with `xivo-dird` of *XiVO PBX* has been enhanced. After upgrade you must
  verify the configuration of your CTI directory Display in *XiVO PBX* as described in :ref:`directories` and :ref:`dird-integration-views`.

.. note:: Integration note: the *Web* and *Desktop Assistant* support only the display of

  * 1 field for name (the one of type *name* in the directory display)
  * 3 numbers (the one of type *number* and the first two of type *callable*)
  * and 1 email

**Callbacks (CCManager)**

* Default csv separator has been changed from pipe '|' to comma ',' for the callback export.


2016.03
=======
No behavior changes.

