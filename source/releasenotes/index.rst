.. _xivosolutions_release:

****************************
XiVO Solutions Release Notes
****************************

2017.LTS2 (Polaris)
===================

.. warning:: This version is not yet released. You should use the current supported version: :ref:`XiVO 2017.LTS1 (Five) version <xivo_2017_lts1>`.


2017.LTS2 Intermediate Versions
-------------------------------

.. toctree::
   :maxdepth: 2

   2017_lts2_iv


.. _xivo_2017_lts1:

2017.LTS1 (Five)
================

+----------------------+----------------+
| Component            | latest ver.    |
+======================+================+
| xucmgt               | 2017.03.04     |
+----------------------+----------------+
| xuc                  | 2017.03.06     |
+----------------------+----------------+
| recording-server     | 2017.03.03     |
+----------------------+----------------+
| spagobi              | 2017.03.02     |
+----------------------+----------------+
| config-mgt           | 2017.03.02     |
+----------------------+----------------+
| pack-reporting       | 2017.03.02     |
+----------------------+----------------+
| xivo-full-stats      | 2017.03.02     |
+----------------------+----------------+
| xivo-db-replication  | 2017.03.06     |
+----------------------+----------------+
| pgxivocc             | 1.3            |
+----------------------+----------------+
| elasticsearch        | 1.7.2          |
+----------------------+----------------+
| xivoxc_nginx         | 0.8            |
+----------------------+----------------+
| fingerboard          | 0.6            |
+----------------------+----------------+
| kibana_volume        | 0.1            |
+----------------------+----------------+
| recording-rsync      | 1.0            |
+----------------------+----------------+
| XiVO PBX             | 2017.03.04     |
+----------------------+----------------+

2017.03.06
----------

.. note:: Bugfix release. See :ref:`2017.03.00 section <2017_03_00_release>` for feature list and behavior changes.

Consult the `2017.03.06 Roadmap <https://projects.xivo.solutions/versions/42>`_ for complete list of fixes.

Components updated: **xuc**, **xivo-db-replication**

**XUC Server**

* `#1074 <https://projects.xivo.solutions/issues/1074>`_ - Xuc AMI connection broken on forward to channel starting with 'Local/\*\*\*'

**Reporting**

* `#1056 <https://projects.xivo.solutions/issues/1056>`_ -  Pack reporting upgrade to latests db version does not alter queue_log table as expected


2017.03.05
----------

.. note:: Bugfix release. See :ref:`2017.03.00 section <2017_03_00_release>` for feature list and behavior changes.

Consult the `2017.03.05 Roadmap <https://projects.xivo.solutions/versions/43>`_ for complete list of fixes.

Components updated: **xuc**

**XUC Server**

* `#1040 <https://projects.xivo.solutions/issues/1040>`_ -  Unable to connect an agent 2 times on different applications

2017.03.04
----------

.. note:: Bugfix release. See :ref:`2017.03.00 section <2017_03_00_release>` for feature list and behavior changes.

Consult the `2017.03.04 Roadmap <https://projects.xivo.solutions/versions/41>`_ for complete list of fixes.

Components updated: **xuc**, **xucmgt**, **xivo-config**, **xivo-libdao**

**XUC Server**

* `#602 <https://projects.xivo.solutions/issues/602>`_ -  Web Agent: outgoing calls are counted as incoming call if Agent issues a call while Paused
* `#935 <https://projects.xivo.solutions/issues/935>`_ -  Call tracking is not working when using a custom Caller Id in the user
* `#1014 <https://projects.xivo.solutions/issues/1014>`_ -  Phone status unknown after Xuc server start, updated on first phone event
* `#1015 <https://projects.xivo.solutions/issues/1015>`_ -  Remove dial timeout on transfer from web assistant

**CCAgent**

* `#937 <https://projects.xivo.solutions/issues/937>`_ -  Wrong message if not an agent when login in ccagent
* `#985 <https://projects.xivo.solutions/issues/985>`_ -  Disable Conference on not supported phones in ccagent
* `#1034 <https://projects.xivo.solutions/issues/1034>`_ -  between CCManager and Web Agent, two columns with the same name have a different meaning

**CC Manager**

* `#1016 <https://projects.xivo.solutions/issues/1016>`_ -  CC coach is not displaying anymore the queues
* `#1024 <https://projects.xivo.solutions/issues/1024>`_ -  CCManager does not display custom agent status
* `#1026 <https://projects.xivo.solutions/issues/1026>`_ -  Checkbox to show Groups in ccmanager is not displayed when ticked

**XiVO PBX**

* `#316 <https://projects.xivo.solutions/issues/316>`_ - When user change context, modification not reported in database
* `#989 <https://projects.xivo.solutions/issues/989>`_ -  Extensions beginning by 00 missing in the default call rights

**XiVO provisionning**

* `#1013 <https://projects.xivo.solutions/issues/1013>`_ -  Move plugin xivo-patton from Supported to Community


2017.03.03
----------

.. note:: Bugfix release. See :ref:`2017.03.00 section <2017_03_00_release>` for feature list and behavior changes.

Consult the `2017.03.03 Roadmap <https://projects.xivo.solutions/versions/39>`_ for complete list of fixes.

Components updated: **xuc**, **xucmgt**, **recording-server**, **xivo-web-interface**

**XUC Server**

* `#947 <https://projects.xivo.solutions/issues/947>`_ - Fix issue when line was edited and avoided xuc user to receive calls
* `#961 <https://projects.xivo.solutions/issues/961>`_ - Fix EventDialing when release Hold in conference room on Polycom and Yealink
* `#960 <https://projects.xivo.solutions/issues/960>`_ - Fix state of Agent when dialing while on pause
* `#953 <https://projects.xivo.solutions/issues/953>`_ - Fix unable to log a user when trying too early at xuc startup

**Desktop Assistant / Web Assistant**

* `#957 <https://projects.xivo.solutions/issues/957>`_ - Fix untranslated error after logout when Xuc down and no automatic re-login
* `#965 <https://projects.xivo.solutions/issues/965>`_ - Fix ringing tone sometimes played only once on Desktop assistant
* `#951 <https://projects.xivo.solutions/issues/951>`_ - Add audio feedback when call is hangup with WebRTC

**XiVO PBX**

* `#971 <https://projects.xivo.solutions/issues/971>`_ - Fix adding new voicemail to Closed hours :ref:`schedule <schedules>`.

**Recording**

* `#796 <https://projects.xivo.solutions/issues/796>`_ - Avoid adding blank number in form

**System**

* `#952 <https://projects.xivo.solutions/issues/952>`_ - Add new shortcut to get all docker container versions (see
  :ref:`admin_version`)::

   xivocc-dcomp version


2017.03.02
----------

.. note:: Bugfix release. See :ref:`2017.03.00 section <2017_03_00_release>` for feature list and behavior changes.

Consult the `2017.03.02 Roadmap <https://projects.xivo.solutions/versions/38>`_ for complete list of fixes.

Components updated: **xuc**, **xucmgt**, **XiVO PBX**, **recording-server**, **spagobi**, **config-mgt**, **pack-reporting**, **xivo-full-stats**, **xivo-db-replication**

**CCAgent**

* `#877 <https://projects.xivo.solutions/issues/877>`_ - Transfer from directory search
* `#802 <https://projects.xivo.solutions/issues/802>`_ - Fix error when calling from directory search
* `#739 <https://projects.xivo.solutions/issues/739>`_ - Fix error when Config mgt is down
* `#217 <https://projects.xivo.solutions/issues/217>`_ - Fix first login attempt may fail

**CCManager / Config mgt**

* `#798 <https://projects.xivo.solutions/issues/798>`_ - Secured websocket detection
* `#847 <https://projects.xivo.solutions/issues/847>`_ - Deleted agent group still appears in ConfigMGT & CCManager

**Desktop Assistant / Web Assistant**

* `#931 <https://projects.xivo.solutions/issues/931>`_ - Auto-updater does not quit if close in tray is enabled
* `#844 <https://projects.xivo.solutions/issues/844>`_ - WebRTC - Conversation still ongoing after Directed Pickup
* `#902 <https://projects.xivo.solutions/issues/902>`_ - Null is displayed as forward number if validate an empty string in forward modal
* `#730 <https://projects.xivo.solutions/issues/730>`_ - Should automatically reconnect after error of pc on
* `#588 <https://projects.xivo.solutions/issues/588>`_ - Answering to headset or loudspeaker based on context (Yealink)
* `#558 <https://projects.xivo.solutions/issues/558>`_ - Forwarding can be set to non-digits
* `#786 <https://projects.xivo.solutions/issues/786>`_ - WebRTC: handle error after multiple aborted incomming calls

**Recording**

* `#200 <https://projects.xivo.solutions/issues/200>`_, `#790 <https://projects.xivo.solutions/issues/790>`_ - ``xivo-recording`` and ``call-recording-filtering`` packages were replaced by ``xivocc-recording``. The subroutines shipped in this new package (see: :ref:`recording_configuration`)
  were updated with new ``xivocc-`` prefix to be able to add them on *XiVO PBX* objects (Incalls, Queues ...) via Web interface.

  .. warning:: If these packages were already installed, you **MUST** follow specific :ref:`upgrade_recording_xpbx`.

**Reporting**

* `#775 <https://projects.xivo.solutions/issues/775>`_ - Handle processing error in xivo-full-stats

**XiVO PBX**

* `#846 <https://projects.xivo.solutions/issues/846>`_ - Cannot open an agent group on the second page if there are more than 20 agent groups and the selected group doesn't have more than 20 agents
* `#777 <https://projects.xivo.solutions/issues/777>`_ - Wizard checkbox is set to False after returning from 4th step
* `#746 <https://projects.xivo.solutions/issues/746>`_ - Fix voicemail creation in import
* `#745 <https://projects.xivo.solutions/issues/745>`_ - Handle login casing in import
* `#744 <https://projects.xivo.solutions/issues/744>`_ - Handle login duplicates in import
* `#714 <https://projects.xivo.solutions/issues/714>`_ - Handle special character in SIP registry
* `#463 <https://projects.xivo.solutions/issues/463>`_ - When a user forward (either `*21` or native terminal softkey) to a queue, the callerid is not prepend accordingly to the queue configuration

**XiVO Provisioning**

* `#939 <https://projects.xivo.solutions/issues/939>`_ - Update Polycom firmware 4.0.9 download URL
* `#776 <https://projects.xivo.solutions/issues/776>`_ - Add new MAC address OUI for Polycom (64:16:7F)

**XUC Server**

* `#942 <https://projects.xivo.solutions/issues/942>`_ - Environment variable AUTH_SECRET is not used by xucserver
* `#609 <https://projects.xivo.solutions/issues/609>`_ - Fix SHOW_CALLBACKS environment variable usage

**System**

* Docker images are now labelled with the exact version of the embedded application. See :ref:`admin_version`.

2017.03.01
----------

.. note:: Bugfix release. See :ref:`2017.03.00 section <2017_03_00_release>` for features list and behavior changes.

Consult the `2017.03.01 Roadmap <https://projects.xivo.solutions/versions/37>`_.

Components updated: **xuc**, **xucmgt**

* Fix LDAP authentication that was no longer working
* Fix auto-update for Desktop Assistant that didn't for Windows
* Fix *Select2Call* key that was not working with secured connection (which includes WebRTC users).
* Update ``xivo-dist`` package to prepare upgrade for future LTS

.. _2017_03_00_release:

2017.03.00
----------

.. note:: **LTS** Release. Below are listed the features, bugfixes and behavior changes.

Consult the `2017.03.00 Roadmap <https://projects.xivo.solutions/versions/13>`_.

**CC Manager**

* CCManager Access management: CCManager access **is now authorized only for users with a with 'Superviseur' or 'Administrateur' rights** (see :ref:`ccmanager-security`).

**Web Assistant / Desktop assistant**

* Name of caller is displayed for incoming calls (when it is known - usually internal users only). When incoming call is
  ringing, name of caller is displayed in popup dialog and in system notification. When call is accepted, name of caller
  is displayed in list of calls.
* Number of missed calls is displayed as number on history button (like number of voice messages on voicemail button).
  Number is cleared once history is displayed.
* Simplification of the forwarding call form by keeping only one field number to configure.
* Added a new header icon to show that you are using **WebRTC** and not a physical phone.
* Added possibility to initiate attended transfer while using **WebRTC** (same UI as for users with physical phones).
* **Desktop Assistant** features only (See :ref:`desktop-assistant` for details):

 * Added **Select2Call** feature with configurable global keyboard shortcut to handle some actions to interact with call.
 * Added `callto:` and `tel:` link support to initiate calls.
 * Added options to close in tray and launch application automatically at startup.

**Xuc Server**

* New Authentication API has been implemented with explicit error handling messages. See :ref:`CTI Authentication <cti_authentication>` for more details.


**XiVO PBX**

* `Asterisk` has been upgraded by default to **13.13.1** version (see `Feature 563
  <https://projects.xivo.solutions/issues/563>`_ for list of fixed bugs).

  .. warning:: This version fixes bug `Calls assigned to busy agents even if ringinuse=0 <https://projects.xivo.solutions/issues/621>`_.
    Then you **SHOULD** take care of removing the palliative ``pre-limit-agentcallback`` subroutine.

* For `asterisk` versions, a new manual procedure is available to switch to oldstable or new candidate one, see :ref:`upgrade_asterisk_latest`.

**System**

* New ``xivocc-dcomp`` alias to manage `XivoCC` docker containers.

  .. warning::
    ``dcomp`` alias was replaced by ``xivocc-dcomp`` script. If you are upgrading from version between 2016.04 and 2017.LTS1 you **must run**::

        unalias dcomp

* Yaml file :file:`/etc/docker/compose/docker-xivocc.yml` now contains all environment variables (like *USE_SSO* etc.).
  Value of these variables are set in two new files:

    * :file:`/etc/docker/compose/factory.env`
    * and :file:`/etc/docker/compose/custom.env`

  To customize XiVO CC edit the ``custom.env`` file.
  The :file:`/etc/docker/compose/.env` file is overwritten every time you run ``xivocc-dcomp`` script with at least one argument.

* To minimize downtime, *XiVO CC* services will not be stopped when upgrading ``xivocc-installer`` package (true for ``xivocc-installer`` package 2017.LTS1 and newer).
* Fix a problem which prevented ``xivocc-installer`` to finish properly if *XiVO PBX* wasn't accessible, even if you had
  choosen not to configure it.
* Upon upgrade, parameter ``ENFORCE_MANAGER_SECURITY`` will be set to ``false`` in file :file:`/etc/docker/compose/custom.env`
  to keep old behavior. If you want to have the new behavior, remove ``ENFORCE_MANAGER_SECURITY`` parameter from the file
  :file:`/etc/docker/compose/custom.env` and recreate the container (do not forget to add *Superviseur* or *Administrateur* rights
  to the users in the Configuration Management server).


2017.LTS1 Intermediate Versions
-------------------------------

.. toctree::
   :maxdepth: 2

   2017_lts1_iv


Archives
========

.. toctree::
   :maxdepth: 2

   old_release_notes
